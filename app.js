const express    = require("express");
const app        = express();
const dotenv     = require("dotenv");
const bodyParser = require('body-parser');

// Importing routes.
const CampaignRoute = require('./routes/CampaignRoute');

// Load env file.
dotenv.config();

// Body Parser middleware - JSON and URL-encoded parser - will be applied to all the routes below.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(CampaignRoute);

// Send 404 error: API not found.
app.use((req, res) => {
    res.status(404).json({
        error: true, 
        message: 'API Not found',
        status: 404,
        data: null
    });
});

module.exports = app;