const fetch = require('node-fetch');
const config = require('../config');

class CampaignService {
    static async getAllCampgains(filters) {
        try {
            const {status, sortAs, sortBy} = filters;

            let campaigns = await this.getCampaignsFromDonateKart();

            // Filter Data
            if (status === 'active') {
                campaigns = this.filterByActiveCampaigns(campaigns);
            } else if (status === 'closed') {
                campaigns = this.filterByClosedCampaigns(campaigns);
            }

            // Sort Data
            campaigns = this.sortCampaigns(campaigns, sortAs, sortBy);

            // Format Data
            campaigns = this.formatCampaigns(campaigns);

            return {count: campaigns.length, campaigns};
        } catch (error) {
            throw new Error(error);
        }
    }

    static async getCampaignsFromDonateKart() {
        try {
            const res = await fetch(config.donatekartEndpoint);

            return (res.ok) ? res.json() : [];
        } catch (error) {
            throw new Error(error);
        }
    }

    static sortCampaigns(campaigns, sortAs = 'totalAmount', sortBy = 'desc') {
        const sort = (sortBy === 'asc') ? 1 : -1;

        return campaigns.sort((a, b) => {
            return (a[sortAs] > b[sortAs]) ? sort * 1 : sort * -1;
        });
    }

    static formatCampaigns(campaigns) {
        return campaigns.map((campaign) => {
            const {title, totalAmount, backersCount, endDate} = campaign;

            return {
                title,
                totalAmount,
                backersCount,
                endDate
            }
        });
    }

    static filterByActiveCampaigns(campaigns) {
        const currentDate = new Date();

        // Checking 2 conditions.
        // 1st Condition -> creaed should be <= 30 days
        // 2nd Codition -> endDate is greater than today.

        return campaigns.filter((campaign) => {
            return (
                this.getDateDiffInDays(currentDate, campaign.created) <= -30 &&
                this.getDateDiffInDays(currentDate, campaign.endDate) > 0
            );
        });
    }

    static filterByClosedCampaigns(campaigns) {
        const currentDate = new Date();

        // Checking 2 conditions.
        // 1st Condition -> prodcureAmount >= totalAmount
        // 2nd Codition -> endDate is less than today.

        return campaigns.filter((campaign) => {
            return (
                campaign.procuredAmount >= campaign.totalAmount ||
                this.getDateDiffInDays(currentDate, campaign.endDate) <= 0
            );
        });
    }

    static getDateDiffInDays(date1, date2) {
        date1 = new Date(date1);
        date2 = new Date(date2);

        let res = date2.getTime() - date1.getTime();

        // Convert difference in days.
        res = Math.ceil(res / (1000 * 60 * 60 * 24));

        return res;
    }
}

module.exports = CampaignService;