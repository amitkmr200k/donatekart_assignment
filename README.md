# DONATEKART - Assignment | Campaigns API

## Getting Started

### Prerequisites

Please make sure you have following installed in your machine.

* Node
* npm
* nodemon (optional)

If not, please install them before moving forward.

---

### Project Steup

Follow the setps given below to setup the project

Steps to follow:
```
1) Clone the repository in project directory
git clone https://gitlab.com/amitkmr200k/donatekart_assignment.git amit_donatekart_assignment

2) Go to project directory
cd amit_donatekart_assignment

3) Replace .env.example file with .env file and enter the details. 

4) From project directory run following command(s):
npm install

5) To run the project, run the following command:
npm start
or
node server.js

If you have nodemon installed run:
npm run dev

6) Now the project is up and running, and you can now make API calls.

```
## API EndPoints

Below are the endpoints for making API calls.

```
1) Get All campaigns
Method: Get
API endpoint: /campaign
```

```
2) Get Active campaigns
Method: Get
API endpoint: /campaign?status=active
```

```
3) Get Closed campaigns
Method: Get
API endpoint: /campaign?status=closed
```
