var express = require('express')
var router = express.Router()

const CampaignController = require('../controllers/CampaignController');

router.get('/campaign', CampaignController.getAllCampaigns);

module.exports = router