const CampaignService     = require('../services/CampaignService');

class CampaignController {
    static async getAllCampaigns(req, res) {
        try {
            const allCampaigns = await CampaignService.getAllCampgains(req.query); 

            return res.status(200).json({
                error: false, 
                status: 200,
                message: 'List of campaigns',
                data: allCampaigns
            });
        } catch (error) {
            return res.status(500).json({
                error: true, 
                status: 500,
                message: `Server Error: ${error}`,
                data: null
            });
        }
    }
}

module.exports = CampaignController;