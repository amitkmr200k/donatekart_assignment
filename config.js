const config = {
    serverPort: process.env.SERVER_PORT || 3000,
    donatekartEndpoint: process.env.DONATEKART_ENDPOINT || 'https://testapi.donatekart.com/api/campaign'
};

module.exports = config;